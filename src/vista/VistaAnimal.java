/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.io.IOException;
import modelo.Animal;
import util.Util;

/**
 * Fichero: VistaPersona.java
 * Ejercicio0319
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 08-nov-2013
 */

public class VistaAnimal {

    public Animal getAnimal() throws IOException {

    Animal alumno = new Animal();
    Vista vista = new Vista();
    Util util = new Util();
    String persona;
    int edad = 0;

    System.out.println("TOMA DE DATOS ANIMAL");

    System.out.print("Nombre: ");
    persona = util.pedirString();
    alumno.setNombre(persona);

    while (edad <= 0) {
      System.out.print("Edad: ");
      edad = util.pedirInt();
      if (edad <=0 ) vista.error(2);
    }
    alumno.setEdad(edad);

    return alumno;
  }

  public void showAnimal(Animal p1) {

    System.out.println("MOSTRANDO DATOS ANIMAL");
    System.out.println(p1.getNombre() + " " + p1.getEdad());


  }
  
  
}
