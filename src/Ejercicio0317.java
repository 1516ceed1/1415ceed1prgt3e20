
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 06-nov-2013
 */
public class Ejercicio0317 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic her

        int suma = 0;
        int numero = 1;
        Scanner sc = new Scanner(System.in);

        for (; numero != 0;) {
            System.out.print("Numero: ");
            numero = sc.nextInt();
            suma = numero + suma;
            if (numero == 0) {
                break;
            }
        }
        System.out.println("La suma es " + suma);
    }
}

/* Ejecucion
 Introduce Numero: 1
 Introduce Numero: 2
 Introduce Numero: 3
 Introduce Numero: 4
 Introduce Numero: 0
 La suma es: 6
 */
